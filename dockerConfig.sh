#!/bin/bash

MY_ADDR=$1
CONSUL_ADDR=$2
DOCKER_LABELS=$3

echo "MY_ADDR=${MY_ADDR}" > /etc/default/swarm-agent
echo "CONSUL_ADDR=${CONSUL_ADDR}" >> /etc/default/swarm-agent
echo "DOCKER_LABELS=\"${DOCKER_LABELS}\"" >> /etc/default/docker

rm -f /etc/docker/key.json

# Restart Docker
service docker restart
systemctl swarm-agent enable
service swarm-agent start

